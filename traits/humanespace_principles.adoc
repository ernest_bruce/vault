*humanespace*
· transparency, dignity, decency
· consistency, robustness, security, safety
· be honest, transparent, authentic, genuine
· be consistent, robust, secure, safe
· seek understanding before being understood
· facilitate reasoning for the long term (https://www.nature.com/articles/nature13530[cooperate with the future])
· classify ideas as either adequate, inadequate, neutral, for the end (Spinoza)
· achieve understanding through experience, discourse, verified information
· facilitate communication, cooperation between groups
· maximize individual autonomy, authority, capability
· keep ends mutually consistent, reciprocally supportive (Kant)
· render components to https://www.w3.org/TR/xhtml1/[XHTML] and https://en.wikipedia.org/wiki/Universal_Scene_Description[Universal Scene Description (USD)] for discovery, exploration, navigation

include::{rel_traits_dir}{humanet_principles}[]
