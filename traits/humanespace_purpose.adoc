*humanespace*
implement a digital world where the ends, needs, wants of participants are paramount
· where for·profit interests do not apply
· where entities can reason about their world, and determine their own lives without interference from outsiders

help entities maximize understanding that takes them closer to their ends
· and minimize things that take them away from those ends

facilitate access to enclosed non·pod commons

include::{rel_traits_dir}{humanet_purpose}[]
