*humanet*
· https://dailystoic.com/what-is-stoicism-a-definition-3-stoic-exercises-to-get-you-started[be stoic] (courageous, temperate, just, wise)
· monetary profit does not influence any aspect of design or operation
· embrace agreeable things that move entities closer to their ends (Kant)
· do not expose details of base technologies (they are replaceable)
· prioritize reliability, security (dependability, robustness) over speed
· place smarts at the edges (https://en.wikipedia.org/wiki/end-to-end_principle[end·to·end design principle])
· trust data by distrusting everything (zero trust, trustless)
